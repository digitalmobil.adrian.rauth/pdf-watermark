# PDF-Watermark

Proof-of-concept. Automatically add watermarks and password protection to pdf files.

## Prequisites

* php 7.1+
* composer

## Setup

```bash
composer install
php run.php
```