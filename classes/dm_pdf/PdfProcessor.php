<?php

namespace dm_pdf;

use League\CLImate\CLImate;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use Mpdf\Output\Destination;
use setasign\Fpdi\PdfParser\PdfParserException;

class PdfProcessor {

  /** @var Climate */
  private $cli;

  /** @var string */
  private $directory_input = __DIR__ . '/../../input/';

  /** @var string */
  private $directory_output = __DIR__ . '/../../output/';

  /**
   * PdfProcessor constructor.
   * @throws MpdfException
   * @throws PdfParserException
   */
  public function __construct() {
    $this->setUpClimate();
    $this->processPdfFiles();
  }

  /**
   * Initial CLImate setup
   *
   * @return void
   */
  private function setUpClimate() {
    $this->cli = new CLImate();
    $this->cli->arguments->add([
      'help' => [
        'prefix'      => 'h',
        'longPrefix'  => 'help',
        'description' => 'Prints Help Message',
        'noValue'     => true,
      ],
    ]);
    $this->cli->arguments->parse();
  }

  /**
   * @throws PdfParserException
   * @throws MpdfException
   */
  private function processPdfFiles() {
    foreach (glob($this->directory_input . '*pdf') as $file) {
      $filename = pathinfo($file, PATHINFO_BASENAME);
      $this->cli->green("Processing '{$filename}' ...");

      $pdf = new Mpdf();
      $pageCount = $pdf->setSourceFile($file);

      for($i = 1; $i <= $pageCount; $i++) {
        $pdf->AddPage();
        $tplIdx = $pdf->importPage($i);
        $pdf->useTemplate($tplIdx, 0, 0);
        #$pdf->SetFont('Arial');
        #$pdf->SetTextColor(255,0,0);
        #$pdf->SetXY(25, 25);
        #$pdf->Write(0, "This is just a simple text");
        $pdf->watermark('foobar');
      }

      $pdf->SetProtection(['extract'], 'foo', 'bar');
      $pdf->Output($this->directory_output . $filename, Destination::FILE);
    }
  }

}