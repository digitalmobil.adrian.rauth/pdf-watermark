<?php

# Force display of errors and set memory limit no matter what is set by the client
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '2048M');

require_once('classes/external/autoload.php');

use dm_pdf\PdfProcessor;

$processor = new PdfProcessor();